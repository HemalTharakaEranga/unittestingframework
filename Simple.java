package experiments;

/******************************************************************************
 * This class is used to demonstrate various aspects of Java reflection. 
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2017
 ******************************************************************************/

public class Simple {

	  public double a = 10.55;
	  private double b = 20.55;

	  public Simple() {
	  }

	  public Simple(double a, double b) {
	    this.a = a;
	    this.b = b;
	  }

	  public void squareA() {
	    this.a *= this.a;
	  }

	  private void squareB() {
	    this.b *= this.b;
	  }

	  public double getA() {
	    return a;
	  }

	  private void setA(double a) {
	    this.a = a;
	  }

	  public double getB() {
	    return b;
	  }

	  public void setB(double b) {
	    this.b = b;
	  }

	  public String toString() {
		  return String.format("Simple(a=%.2f, b=%.2f)", a, b);

  }
}
